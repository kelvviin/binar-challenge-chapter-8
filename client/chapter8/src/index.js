import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Register from './Register';
import Edit from './Edit';
import Search from './Search';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
      <div>
        {/* <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/create">Create</Link>
            </li>
            <li>
              <Link to="/edit">Edit</Link>
            </li>
            <li>
              <Link to="/search">Search</Link>
            </li>
          </ul>
        </nav> */}

          <nav class="navbar navbar-dark navbar-expand-lg bg-dark sticky-top">
              <Link to="/" class="navbar-brand">Home</Link>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                      <li class="nav-item">
                      <Link to="/create" class="nav-link">Create</Link>
                      </li>
                      <li class="nav-item">
                      <Link to="/edit" class="nav-link">Edit</Link>

                      </li>
                      <li class="nav-item">
                      <Link to="/search" class="nav-link">Search</Link>
                      </li>
                  </ul>
              </div>
          </nav>
          <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>

        <Routes>
          <Route path="/" element={<App/>}>
          </Route>
          <Route path="/create" element={<Register/>}>
          </Route>
          <Route path="/edit" element={<Edit/>}>
            
          </Route>
          <Route path="/search" element={<Search/>}>
            
          </Route>
        </Routes>
      </div>
    </Router>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
