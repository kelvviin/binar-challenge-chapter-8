import React,{Component} from 'react';

class Edit extends Component{
    constructor(props){
        super(props);
        this.state={
            username:"",
            email:"",
            experience:0,
            lvl:0
        }
        
        this.handleChange = this.handleChange.bind(this);
    }

    submit = () =>{
        console.log("Submit",this.state);

    }

    handleChange(event){
        console.log("event",event)
        const name = event.target.name
        this.setState({[name]:event.target.value})
    }

    render(){
        return(
            <>
                <div class="container-fluid my-4">
                <form action="" method="">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" name="username" value={this.setState.username} onChange={this.handleChange} class="form-control" placeholder="Enter username"/>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" name="email" value={this.setState.email} onChange={this.handleChange} class="form-control" placeholder="Enter email"/>
                    </div>
                    <div class="form-group mb-3">
                        <label for="experience">EXP</label>
                        <input type="number" name="experience" value={this.setState.experience} onChange={this.handleChange} class="form-control" placeholder="Enter experience"/>
                    </div>
                    <button type="button" onClick={this.submit} class="btn btn-success">Edit</button>
                </form>
            </div>
            </>
        );
    }
}

export default Edit;